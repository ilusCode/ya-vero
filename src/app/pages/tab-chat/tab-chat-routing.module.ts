import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabChatPage } from './tab-chat.page';

const routes: Routes = [
  {
    path: '',
    component: TabChatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabChatPageRoutingModule {}
