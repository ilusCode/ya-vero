import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/interfaces/profile';
@Component({
  selector: 'app-tab-chat',
  templateUrl: './tab-chat.page.html',
  styleUrls: ['./tab-chat.page.scss'],
})
export class TabChatPage {
  contacts: Profile[] = [
    {
      uuid: '1',
      username: 'prueba_1',
      image:
        'https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y',
      state: 'Pensando en ti',
      phone: '1234567890',
      in_line: true,
    },
    {
      uuid: '2',
      username: 'prueba_2',
      image:
        'https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7721?d=identicon&f=y',
      state: 'Pensando en ti',
      phone: '1234567890',
      in_line: false,
    },
    {
      uuid: '3',
      username: 'prueba_3',
      image:
        'https://gravatar.com/avatar/dba2bae8c566f9d4041fb9cd9ada7721?d=identicon&f=y',
      state: 'Pensando en ti',
      phone: '1234567890',
      in_line: true,
    },
  ];

  constructor() {}

  openChat(id: string) {
    console.log('id ->', id);
  }
  viewProfileById(id: string) {
    var obj: any = this.contacts.filter((i) => i.uuid === id);
    console.log(obj[0]);
  }
}
