export interface Profile {
  uuid?: string;
  username: string;
  image: string;
  state: string;
  phone: string;
  in_line: boolean;
}
